# Twitch Stream Capture

This is a Python script that allows you to capture Twitch streams for a particular streamer using `ffmpeg`. The script also logs the entire capture process for debugging or record-keeping purposes.

## Terms

Before downloading any content from this repository, please make sure to familiarize yourself with the Twitch.tv Terms of Service. The content available on Twitch.tv is subject to their terms and conditions, and it is your responsibility to ensure compliance.

Downloading or using Twitch.tv content without proper authorization or in violation of their terms may result in legal consequences. Be mindful of any licensing restrictions, copyright issues, or usage limitations imposed by Twitch.tv on their content.

Please note that this repository is not affiliated with Twitch.tv and does not assume any responsibility for your usage of Twitch.tv content. By downloading and using any content from this repository, you agree to be solely responsible for adhering to Twitch.tv's Terms of Service.

For more information, please visit the [Twitch.tv Terms of Service](https://www.twitch.tv/p/legal/terms-of-service/) page.

## Requirements

- Python 3.x
- ffmpeg (Installed and available in PATH)
- streamlink
- moviepy

## Installation

1. Install Python 3.x and pip if you haven't already. You can download it from [here](https://www.python.org/downloads/).

2. Install ffmpeg. You can download it from [here](https://www.ffmpeg.org/download.html).

3. Once you have Python and ffmpeg installed, you can install the required Python libraries using pip:

```bash
pip install -r requrements.txt
```

## Usage

1. Run the script

```bash
python twitch_capture.py
```

2. Enter the Twitch streamer's ID when prompted:

```bash
Enter streamer ID: 
```

3. To stop the capture process, press Enter.

## Features

- **Streamer ID Input**: Promptly requests the streamer's ID for capturing.
- **Stream Quality Selection**: Retrieves the best available stream for the specified channel.
- **Stream Downloading via ffmpeg**: Utilizes ffmpeg for downloading the stream efficiently.
- **Timestamped File Saving**: Saves the captured stream as an MP4 file, including the timestamp and channel name in the filename.
- **Detailed Logging**: Logs ffmpeg output for debugging and record-keeping.
- **Error and Interruption Handling**: Manages interruptions and errors during the capture process.
- **User-Controlled Capture Termination**: Allows the user to stop the capture process by pressing Enter.
- **Video Duration Analysis with Moviepy**: Incorporates moviepy to provide the duration of the captured content, enhancing the tool's functionality.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
This script is released under the MIT License.
